package SM703Project;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class SM703ProjectTest {

	 
		@Test
		public void testAddPositiveNumbers() {
		
			assertEquals(SM703Project.calculate(new int[] {5, 5,10}), 20);
		}
		
		@Test
		public void testAddPositiveandNegativeNumbers() {
		
			assertEquals(SM703Project.calculate(new int[] {-5, 5,10}), 10);
		}	

	
		@Test
		public void testAddZero() {
		
			assertEquals(SM703Project.calculate(new int[] {0}), 0);
		}
		
		@Test
		public void testAddNegativeNumbers() {
		
			assertEquals(SM703Project.calculate(new int[] {-1,-3,-7}), -11);
		}
		
				
		@Test
		void test_RealFail() {
			assertNotEquals(SM703Project.calculate(new int[] {5, 5,10}),10);
		} 
		
		@Test
		void test_Fail() {
			assertNotEquals(SM703Project.calculate(new int[] {5, 5,10}),20);
		} 
}